<?php
class TMv_InstagramSettingsForm extends TSv_ModuleSettingsForm
{
	public function __construct($module)
	{	
		parent::__construct($module);
		
	}
	
	public function configureFormElements()
	{
//		$field = new TCv_FormItem_TextField('webhook_url', 'Incoming Webhook URL');
//		$field->setHelpText('The required webhook in order for Slack connectivity to work. For more information,
//		learn about <a href="https://my.slack.com/services/new/incoming-webhook">Slack Incoming Webhooks</a>. ');
//		$this->attachView($field);
//
//
//		$field = new TCv_FormItem_HTML('webhook_test', 'Test');
//		$field->addText('<a href="/admin/slack/do/test-default-webhook/">Test Webhook</a>');
//		$this->attachView($field);

		$field = new TCv_FormItem_TextField('access_token','Access Token');
		$field->setHelpText('<a href="http://instagram.pixelunion.net/" target="_blank">Get An Access Token</a>
		<br />Make sure you are logged in as the correct user.');
		$field->setIsRequired();
		$this->attachView($field);

//		$field = new TCv_FormItem_TextField('user_id','User ID');
//		$field->setHelpText('<a href="https://smashballoon.com/instagram-feed/find-instagram-user-id" target="_blank">Find Your User ID</a>
//		<br />The Username you look up must match the user that was logged in when you got the access token. ');
//		$field->setIsRequired();
//		$this->attachView($field);



	}
	

}
?>