<?php

/**
 * Class TMv_InstaFeed
 * @see https://github.com/stevenschobert/instafeed.js
 */
class TMv_InstaFeed extends TCv_View
{
	use TMt_PagesContentView;

	protected $properties = array();
	protected $access_token = '';
	protected $instagram_user_id = '';
	protected $template = '<a href="{{link}}" target="_blank"><img src="{{image}}" /><span class="caption">{{caption}}</span><span class="response_block"><span class="likes"><i class="fa fa-heart"></i> {{likes}}</span><span class="comments"><i class="fa fa-comments"></i> {{comments}}</span></span></a>' ;
	protected $num_photos = 3;
	protected$size;

    /**
     * TMv_InstaFeed constructor.
	 * @param string|bool $id
     */
    public function __construct($id = 'insta_feed')
    {
        parent::__construct($id);

        $this->addClassCSSFile('TMv_InstaFeed?v2');

	}

	/**
	 * @param string $template
	 */
	public function setTemplate($template)
	{
		$this->template = $template;
	}

	/**
	 * @param int $num_photos
	 */
	public function setNumPhotos($num_photos)
	{
		$this->num_photos = $num_photos;
	}

	/**
	 * @return array
	 */
    protected function properties()
	{
		return $this->properties;
	}

    public function render()
	{
		$access_token = TC_getModuleConfig('instagram','access_token');

		// User ID is the first part of the access token
		$user_id = explode('.',$access_token);
		$user_id = $user_id[0];

		// Prime the most basic targets
		$this->properties['target'] = $this->attributeID();
		$this->properties['accessToken'] = $access_token;
		$this->properties['get'] = 'user';
		$this->properties['userId'] = $user_id;
		$this->properties['template'] = $this->template;
		$this->properties['limit'] = $this->num_photos;
		$this->properties['resolution'] = 'standard_resolution';


		// Include Instafeed JS
		$this->addJSFile('instafeed',$this->classFolderFromRoot().'/instafeed.min.js');
//
		if($access_token != '')
		{
			$script = "var feed = new Instafeed({";
			$parts = array();
			foreach($this->properties() as $name => $value)
			{
				$parts[] = $name . ": '" . $value."'";
			}

			$script .= implode(', ', $parts);
			$script .= "	});
			feed.run();";
			$this->addJSLine('instafeed_' . $this->id(), $script);
		}
	}

	//////////////////////////////////////////////////////
	//
	// TMt_PagesContentView TRAIT
	//
	// Settings related to the content view traits
	//
	//////////////////////////////////////////////////////

	/**
	 * The page content form items
	 * @return TCv_FormItem[]
	 */
	public function pageContent_EditorFormItems()
	{
		$form_items = array();


		$field = new TCv_FormItem_HTML('explanation','');
		$field->addText("<p>This plugin uses a 3rd-party tool which will grab the Instagram photos and display them. 
		In order to do this, the website needs permission to access that information. This means you need an access token and user id from Instagram. </p>");
		$field->addText('<p><a href="/admin/instagram/do/settings/">Edit Values – Module Settings</a></p>');
		$form_items[] = $field;

		$field = new TCv_FormItem_HTML('access_token','Access Token');
		$access_token = TC_getModuleConfig('instagram','access_token');
		if($access_token != '')
		{
			$field->addText($access_token);
		}
		else
		{
			$field->addText('Not Set');
		}
		$form_items[] = $field;

		$field = new TCv_FormItem_TextBox('template','Template');
		$field->setHelpText('<a href="https://github.com/stevenschobert/instafeed.js#user-content-templating" target="_blank">Learn About Templates</a>
		<br />You can control what is displayed by the plugin.');
		$field->setIsRequired();
		$field->setDefaultValue('<a href="{{link}}" target="_blank"><img src="{{image}}" /><span class="caption">{{caption}}</span><span class="response_block"><span class="likes"><i class="fa fa-heart"></i> {{likes}}</span><span class="comments"><i class="fa fa-comments"></i> {{comments}}</span></span></a>');
		$form_items[] = $field;

		$field = new TCv_FormItem_Select('num_photos','Number of Photos');
		$field->setHelpText('Indicate the number of photos to be acquired. ');
		$field->setIsRequired();
		$field->setOptionsWithNumberRange(1,60);
		$form_items[] = $field;

		return $form_items;
	}


	public static function pageContent_ViewTitle() { return 'Instagram Feed'; }
	public static function pageContent_ShowPreviewInBuilder() { return true; }

	public static function pageContent_ViewDescription()
	{
		return 'An integrated Instagram Feed.';
	}
}