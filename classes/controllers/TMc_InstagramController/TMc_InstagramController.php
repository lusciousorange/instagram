<?php

/**
 * Class TMc_InstagramController
 */
class TMc_InstagramController extends TSc_ModuleController
{
	/**
	 * TMc_InstagramController constructor.
	 * @param int|string|TSm_Module $module
	 */
	public function __construct($module)
	{
		parent::__construct($module);

	}

	/**
	 * Defines the URL targets
	 */
	public function defineURLTargets()
	{
		// Redirect to Settings
		/** @var TSm_ModuleURLTarget $target */
		$target = TC_initClass('TSm_ModuleURLTarget', '');
		$target->setNextURLTarget('settings');
		$this->addModuleURLTarget($target);

	}



}
?>