Instagram Module
==
This module is used to connect to an instagram account and to show a grid of instagram photos from the account.

Setup & Installation
--
1. Create a folder in `/admin/` called `instagram`. 
   
   *Do not* copy the entire repo folder and rename it. It has hidden `.git` files which will result in the files not appearing in the repo. 
1. Copy contents of the module folder into the `instagram` folder
1. Go to `System` and install the module
1. Go to the Instagram module settings, by clicking on the Gear icon 
1. Contact the client and send them the Pixel Union Website  

   [http://instagram.pixelunion.net/](http://instagram.pixelunion.net/)
1. Ask them to `Generate Access Token`
1. Enter that access token into the settings for the module.

At this point, the module is configured to have access to their Instagram Account

Adding the Instagram Page Content View
--
The module has a view called `TMv_InstaFeed` which can be added to a page.
1. Go to the `Pages` module and add the content to the page
1. Configure the template to show information necessary

   [https://github.com/stevenschobert/instafeed.js#user-content-templating](https://github.com/stevenschobert/instafeed.js#user-content-templating) 
   
2. Add styling as necessary to the site template OR extend the view and override it in `TC_config.php`    